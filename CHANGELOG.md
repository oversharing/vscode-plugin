# Change Log

All notable changes to the "Oversharing" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [Unreleased]

### Changed

- Main section in README.md

## [1.0.0] - 2019-05-21

- Initial release