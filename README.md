# Oversharing

Make sharing live session much easier by generating persistent link instead of randomly generated one.

Example:

    https://oversharing.azurewebsites.net/api/session/a18ec257d58b9c2b6429ac91611a3723318f54e51935c67e7c38a65660bdfb08

This link will not change and will redirect to any new session you
share with Oversharing.

## How to use it

Open editor, start your live session by pressing `Ctrl+Shift+p` and
running command `Oversharing: Share session`.

## Commands

* `Oversharing: Share session` Start a live session.

## Issues/Requests

Please file all issues or requests [here](https://gitlab.com/oversharing/vscode-plugin/issues).

**Enjoy!**